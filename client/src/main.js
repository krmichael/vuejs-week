import Vue from "vue";
import axios from "axios/dist/axios";
import VueRouter from "vue-router";
import Vuex from "vuex";

import App from "./App.vue";
import DomainList from "./components/DomainList";
import DomainView from "./components/DomainView";

Vue.config.productionTip = false;
Vue.use(VueRouter);
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    prefixes: [],
    sufixes: []
  },
  //Responsavel por fazer a multação do estado
  mutations: {
    addPrefix(state, payload) {
      const newPrefix = payload;
      state.prefixes.push(newPrefix.description);
    },
    getPrefixes(state, payload) {
      const { data } = payload;
      state.prefixes = data;
    },
    getSufixes(state, payload) {
      const { data } = payload;
      state.sufixes = data;
    }
  },
  //Responsave por interagir com operações assincronas
  actions: {
    async addPrefix(context, payload) {
      const prefix = payload;

      axios({
        url: "http://localhost:4000",
        method: "post",
        data: {
          query: `
            mutation ($item: ItemInput) {
              newPrefix: saveItem(item: $item) {
                id
                type
                description
              }
            }
          `,
          variables: {
            item: {
              type: "prefix",
              description: prefix
            }
          }
        }
      }).then(response => {
        const query = response.data;
        const newPrefix = query.data.newPrefix;
        context.commit("addPrefix", newPrefix);
      });
    },
    addSufix(context, payload) {
      const sufix = payload;

      context.state.sufixes.push(sufix);
    },
    deleteSufix(context, payload) {
      const sufix = payload;

      context.state.sufixes.splice(context.state.sufixes.indexOf(sufix), 1);
    },
    async getPrefixes(context) {
      axios({
        url: "http://localhost:4000",
        method: "post",
        data: {
          query: `
          {
            prefixes: items (type: "prefix") {
              id
              type
              description
            }
          }
        `
        }
      }).then(response => {
        const query = response.data;
        context.commit("getPrefixes", { data: query.data.prefixes });
      });
    },
    async getSufixes(context) {
      axios({
        url: "http://localhost:4000",
        method: "post",
        data: {
          query: `
          {
            sufixes: items (type: "sufix") {
              description
            }
          }
        `
        }
      }).then(response => {
        const query = response.data;
        context.commit("getSufixes", { data: query.data.sufixes });
      });
    },
    async deletePrefix(context, payload) {
      const prefix = payload;

      axios({
        url: "http://localhost:4000",
        method: "post",
        data: {
          query: `
            mutation ($id: Int) {
              deleted: deleteItem(id: $id)
            }
          `,
          variables: {
            id: prefix.id
          }
        }
      }).then(() => context.dispatch("getPrefixes"));
    }
  }
});

const router = new VueRouter({
  routes: [
    {
      path: "/",
      redirect: "/domains"
    },
    {
      path: "/domains",
      component: DomainList
    },
    {
      path: "/domains/:domain",
      component: DomainView,
      props: true
    }
  ]
});

new Vue({ router, store, render: h => h(App) }).$mount("#app");
